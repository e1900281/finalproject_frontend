/* eslint-disable import/no-anonymous-default-export */

import axios from 'axios';

const getAll = async () => {
  const response = await axios.get ('/available');
  return response.data;
};

const createHotel = async newRestingArea => {
  const response = await axios.post ('/new', newRestingArea);
  return response.data;
};

export default {getAll, createHotel};
