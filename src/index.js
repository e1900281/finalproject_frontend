import App from './App';
import {Provider} from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom/client';
import accommodationReducer from './slice/accommodation';
import {configureStore} from '@reduxjs/toolkit';
import filterReducer from './slice/filter';
const store = configureStore ({
  reducer: {
    roomList: accommodationReducer,
    filter: filterReducer,
  },
});
console.log (store.getState ());
const root = ReactDOM.createRoot (document.getElementById ('root'));
root.render (
  <Provider store={store}>
    <App />
  </Provider>
);
