import {
  Box,
  FormControl,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';

const ListOfRooms = [
  '1 single bed',
  '1 double bed',
  '2 single beds',
  '4 single beds',
  '6 single beds',
  '8 single beds',
  '12 single beds',
];

const RoomDetails = ({room, handleChange}) => {
  return (
    <Box sx={{display: 'flex', flexWrap: 'wrap', width: '70%'}}>
      <FormControl sx={{m: 1, width: '40%'}}>
        <InputLabel>Room type</InputLabel>
        <Select
          name="type"
          value={room.type}
          label="Age"
          onChange={handleChange}
        >
          {ListOfRooms.map (obj => (
            <MenuItem key={obj} value={obj}>{obj}</MenuItem>
          ))}
        </Select>
      </FormControl>
      <TextField
        name="quantity"
        label="Quantity"
        id="outlined-number"
        sx={{m: 1, width: '20%'}}
        value={room.quantity}
        onChange={handleChange}
      />
      <TextField
        name="price"
        label="Price"
        id="outlined-number"
        sx={{m: 1, width: '20%'}}
        value={room.price}
        InputProps={{
          startAdornment: <InputAdornment position="start">€</InputAdornment>,
        }}
        onChange={handleChange}
      />
    </Box>
  );
};

export default RoomDetails;
