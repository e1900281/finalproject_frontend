import {
  Box,
  Button,
  FormControlLabel,
  Radio,
  RadioGroup,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';

import {Link} from 'react-router-dom';
import RoomDetails from './RoomsDetails';
import {createNewHotel} from '../slice/accommodation';
import {useDispatch} from 'react-redux';
import {useState} from 'react';

const NewHotelForm = () => {
  const [newHotel, setNewHotel] = useState ({
    name: '',
    address: '',
    city: '',
    postalCode: '',
    type: '',
  });
  const [room, setRoom] = useState ({
    type: '',
    quantity: 0,
    price: 0,
  });
  const [rooms, setRooms] = useState ([]);
  const dispatch = useDispatch ();
  const handleChange = event => {
    setNewHotel ({...newHotel, [event.target.name]: event.target.value});
  };
  const handleChangeRoom = event => {
    setRoom ({...room, [event.target.name]: event.target.value});
  };
  const addRoom = () => {
    setRooms ([...rooms, room]);
  };
  const submitAction = async event => {
    event.preventDefault ();
    dispatch (createNewHotel (newHotel, rooms));
    setNewHotel ({
      type: '',
      quantity: 0,
      price: 0,
    });
    setRoom ({
      name: '',
      address: '',
      city: '',
      postalCode: '',
      type: '',
    });
    setRooms ([]);
  };
  return (
    <div>
      <form>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <Typography variant="h6">
                  Basic information
                </Typography>
              </TableCell>
              <TableCell
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  flexWrap: 'wrap',
                }}
              >

                <TextField
                  required
                  name="name"
                  label="Name"
                  id="outlined-required"
                  sx={{m: 1, width: '40%'}}
                  value={newHotel.name}
                  onChange={handleChange}
                />
                <TextField
                  required
                  name="address"
                  label="Address"
                  id="outlined-required"
                  sx={{m: 1, width: '40%'}}
                  value={newHotel.address}
                  onChange={handleChange}
                />
                <TextField
                  required
                  name="city"
                  label="City"
                  id="outlined-required"
                  sx={{m: 1, width: '40%'}}
                  value={newHotel.city}
                  onChange={handleChange}
                />
                <TextField
                  required
                  name="postalCode"
                  label="Postal code"
                  id="outlined-required"
                  sx={{m: 1, width: '40%'}}
                  value={newHotel.postalCode}
                  onChange={handleChange}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography variant="h6" sx={{mr: 2}}>Select type:</Typography>
              </TableCell>
              <TableCell>
                <RadioGroup
                  row
                  name="type"
                  value={newHotel.type}
                  onChange={handleChange}
                >
                  <FormControlLabel
                    value="hotel"
                    control={<Radio />}
                    label="Hotel"
                  />
                  <FormControlLabel
                    value="airbnb"
                    control={<Radio />}
                    label="AirBnB"
                  />
                </RadioGroup>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <Typography variant="h6" sx={{mr: 2}}>
                  List of rooms available
                </Typography>
              </TableCell>
              <TableCell>
                <Box sx={{display: 'flex'}}>
                  <RoomDetails room={room} handleChange={handleChangeRoom} />
                </Box>
                <Box sx={{display: 'flex', justifyContent: 'flex-end', mr: 8}}>
                  <Button
                    variant="contained"
                    onClick={addRoom}
                    sx={{my: 2, mx: 0.5}}
                  >
                    Save rooms
                  </Button>
                </Box>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <Button variant="contained" onClick={submitAction}>
          <Link style={{textDecoration: 'none'}} to="/">
            Add
          </Link>
        </Button>

      </form>
    </div>
  );
};

export default NewHotelForm;
