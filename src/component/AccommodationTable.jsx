import {Table, TableBody, TableCell, TableRow, Typography} from '@mui/material';

import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import {Link} from 'react-router-dom';

const displayTypeAvailable = arr => {
  let result = '';
  if (!arr) {
    return 0;
  } else {
    arr.forEach (obj => {
      if (obj.quantity > 0) {
        result += obj.type + ', ';
      }
    });
  }
  return result;
};

const AccommodationTable = ({roomList}) => {
  return (
    <div>
      <p>List of available rooms</p>
      <Table>
        <TableBody>
          {roomList.map (obj => {
            return (
              <TableRow key={obj.id}>
                <TableCell align="left">
                  <Typography variant="h6">{obj.name}</Typography>
                  <Typography variant="body1">
                    {obj.city}
                  </Typography>
                </TableCell>
                <TableCell>
                  {displayTypeAvailable (obj.rooms)}
                </TableCell>
                <TableCell>
                  <Link to={`/available/${obj.id}`}>
                    <Typography variant="body1">
                      More info
                    </Typography>
                    <ArrowForwardIcon />
                  </Link>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </div>
  );
};

export default AccommodationTable;
