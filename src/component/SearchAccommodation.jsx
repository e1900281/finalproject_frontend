import {useDispatch, useSelector} from 'react-redux';

import AccommodationTable from './AccommodationTable';
import FilterForm from './FilterForm';
import {getTable} from '../slice/accommodation';
import {useEffect} from 'react';

const SearchAccommodation = () => {
  const dispatch = useDispatch ();

  useEffect (
    () => {
      dispatch (getTable ());
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );
  const roomList = useSelector (state => state.roomList);

  return (
    <div>
      <FilterForm />
      <AccommodationTable roomList={roomList} />
    </div>
  );
};

export default SearchAccommodation;
