import {Link, useParams} from 'react-router-dom';
import React, {useState} from 'react';
import {Table, TableBody, TableCell, TableHead, TableRow} from '@mui/material';
import {useDispatch, useSelector} from 'react-redux';

import {getTable} from '../slice/accommodation';

const Details = () => {
  const id = useParams ().id;
  const dispatch = useDispatch ();
  useState (() => {
    dispatch (getTable ());
  }, []);
  const roomList = useSelector (state => state.roomList);
  const hotel = roomList.find (obj => obj.id === Number (id));

  return (
    <div>
      <Link to="/">Back</Link>
      <h1>{hotel.name}</h1>
      <p>{hotel.address}</p>
      <p>List of remaining rooms</p>
      <Table>
        <TableHead>
          <TableCell>Type</TableCell>
          <TableCell>Quantity</TableCell>
          <TableCell>Price per night (EUR)</TableCell>
        </TableHead>
        <TableBody>
          {hotel.rooms.map (
            obj =>
              obj.quantity > 0 &&
              <TableRow key={obj.type}>
                <TableCell align="left">
                  {obj.type}
                </TableCell>
                <TableCell>
                  {obj.quantity}
                </TableCell>
                <TableCell>
                  {obj.price}
                </TableCell>
              </TableRow>
          )}
        </TableBody>
      </Table>

    </div>
  );
};

export default Details;
