import {
  Box,
  Button,
  Card,
  FormControl,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';

import SearchIcon from '@mui/icons-material/Search';
import {getFilter} from '../slice/filter';
import {useDispatch} from 'react-redux';
import {useState} from 'react';

const FilterForm = () => {
  const dispatch = useDispatch ();
  const [city, setCity] = useState ('');
  const [houseType, setHouseType] = useState ('');
  const [maxPrice, setMaxPrice] = useState ('');
  const submitAction = () => {
    dispatch (getFilter (city, houseType, maxPrice));
    setCity ('');
    setHouseType ('');
    setMaxPrice ('');
  };
  const handleChange = event => {
    switch (event.target.name) {
      case 'city':
        setCity (event.target.value);
        break;
      case 'type':
        setHouseType (event.target.value);
        break;
      case 'price':
        setMaxPrice (event.target.value);
        break;
      default:
        break;
    }
  };

  return (
    <Card>
      <form onSubmit={submitAction}>
        <Box sx={{display: 'flex', justifyContent: 'center'}}>
          <FormControl sx={{width: '30%', m: 1}}>
            <InputLabel>City's name</InputLabel>
            <Select
              required
              name="city"
              value={city}
              label="City's name"
              onChange={handleChange}
            >
              <MenuItem value={'Vaasa'}>Vaasa, Finland</MenuItem>
              <MenuItem value={'Helsinki'}>Helsinki, Finland</MenuItem>
            </Select>
          </FormControl>
          <FormControl sx={{width: '30%', m: 1}}>
            <InputLabel>Type of accommodation</InputLabel>
            <Select
              name="type"
              value={houseType}
              label="Type of accommodation"
              onChange={handleChange}
            >
              <MenuItem value={'hotel'}>
                Hotel
              </MenuItem>
              <MenuItem value={'airbnb'}>
                AirBnB
              </MenuItem>
            </Select>
          </FormControl>
          <TextField
            name="price"
            label="Maximum price"
            id="outlined-start-adornment"
            sx={{m: 1, width: '20%'}}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">€</InputAdornment>
              ),
            }}
            value={maxPrice}
          />
          <Button variant="contained" sx={{m: 2}}>
            <SearchIcon />
            Search
          </Button>
        </Box>
      </form>
    </Card>
  );
};

export default FilterForm;
