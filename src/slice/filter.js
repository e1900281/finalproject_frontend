import {createSlice} from '@reduxjs/toolkit';

const initialState = {};

const filterSlice = createSlice ({
  name: 'filter',
  initialState,
  reducers: {
    setFilter (state, action) {
      return action.payload;
    },
  },
});

export const {setFilter} = filterSlice.actions;

export const getFilter = (city, type, maxPrice) => {
  return dispatch => {
    dispatch ({
      cityName: city,
      type: type,
      maxPrice: maxPrice,
    });
  };
};

export default filterSlice.reducer;
