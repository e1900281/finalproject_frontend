import {createSlice} from '@reduxjs/toolkit';
import roomService from '../service/room';

const initialState = [];

const accommodationSlice = createSlice ({
  name: 'accommodations',
  initialState,
  reducers: {
    setAllRoomList (state, action) {
      return action.payload;
    },
    setNewRoom (state, action) {
      state.push (action.payload);
    },
  },
});

export const {setAllRoomList, setNewRoom} = accommodationSlice.actions;

export const getTable = () => {
  return async dispatch => {
    const roomList = await roomService.getAll ();
    dispatch (setAllRoomList (roomList));
  };
};

export const createNewHotel = (hotelData, roomList) => {
  return async dispatch => {
    const newRoom = await roomService.createHotel ({
      ...hotelData,
      address: hotelData.address +
        ', ' +
        hotelData.postalCode +
        ', ' +
        hotelData.city,
      rooms: roomList,
    });
    dispatch (setNewRoom (newRoom));
  };
};

export default accommodationSlice.reducer;
