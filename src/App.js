import {Link, Route, BrowserRouter as Router, Routes} from 'react-router-dom';

import Details from './component/Details';
import NewHotelForm from './component/NewHotel';
import SearchAccommodation from './component/SearchAccommodation';
import {Typography} from '@mui/material';

const App = () => {
  return (
    <Router>
      <div>
        <Typography variant="h4">Search hotels/AirBnb easily</Typography>
        <Link style={{padding: 5}} to="/">Home</Link>
        <Link style={{padding: 5}} to="/new">New</Link>
      </div>
      <Routes>
        <Route path="/available/:id" element={<Details />} />

        <Route path="/new" element={<NewHotelForm />} />
        <Route path="/" element={<SearchAccommodation />} />
      </Routes>
    </Router>
  );
};

export default App;
